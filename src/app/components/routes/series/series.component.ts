import { Component, OnInit } from '@angular/core';
import { MoviesSeries } from '../../interface/MovieSeries';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {
  buscar: string = ""
  filter: string = "all"
  //Declaro la variable de la interfaz con su arreglo.
  movies_series: MoviesSeries[] = [
    {
      id: 4,
      name: "How I Met Your Mother",
      description: "Linda película para ver",
      image: "../../../../assets/portadas/img4.png",
      rating: 8.3,
      category: "serie",
    },
    {
      id: 5,
      name: "Money Heist",
      description: "Linda película para ver",
      image: "../../../../assets/portadas/img5.png",
      rating: 8.3,
      category: "serie",
    },
    {
      id: 6,
      name: "Friends",
      description: "Linda película para ver",
      image: "../../../../assets/portadas/img6.png",
      rating: 8.8,
      category: "serie",
    },
    {
      id: 7,
      name: "The Big Bang Theory",
      description: "Linda película para ver",
      image: "../../../../assets/portadas/img7.png",
      rating: 8.1,
      category: "serie",
    },
    {
      id: 8,
      name: "Two And a Half Man",
      description: "Linda película para ver",
      image: "../../../../assets/portadas/img8.png",
      rating: 7,
      category: "serie",
    }

  ]


  series: MoviesSeries[] = this.movies_series
  constructor() { }

  ngOnInit(): void {

  }
  mostrar(filter: string) {
    this.filter = filter

    if (filter == "pelicula") {
      this.series = this.movies_series.filter(movie => (movie.category == "pelicula"))
    }
    else if (filter == "serie") {
      this.series = this.movies_series.filter(movie => (movie.category == "serie"))
    } else {
      this.series = this.movies_series
    }
  }
  buscador() {
    this.series = this.movies_series.filter(movie => movie.name.toLowerCase().includes(this.buscar.toLowerCase()))
  }
  count() {
    if (this.filter == "all") {
      return this.movies_series.length
    }
    let total: number = 0
    this.movies_series.map(movie => {
      if (movie.category == this.filter) {
        total++
      }
    })
    return total
  }
}
